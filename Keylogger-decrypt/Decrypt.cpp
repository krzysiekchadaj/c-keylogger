#include "decrypt.h"

#include <vector>

std::string Base64::Decrypt(const std::string &s)
{
    std::string ret(s);
    ret = ret.erase(7, 1);
    ret = ret.erase(1, 1);
    ret = Decode(ret);
    ret = ret.substr(SALT2.length());
    ret = ret.substr(0, ret.length() - SALT1.length() - SALT3.length());
    ret = Decode(ret);
    ret = ret.substr(0, ret.length() - SALT1.length());
    ret = ret.erase(7, SALT3.length());
    ret = Decode(ret);
    ret = ret.substr(SALT1.length());
    ret = ret.substr(0, ret.length() - SALT2.length() - SALT3.length());
    return ret;
}

std::string Base64::Decode(const std::string &s)
{
    std::string ret;
    std::vector<int> vec(256, -1);
    for (int i = 0; i < 64; i++)
        vec[BASE64_CODES[i]] = i;
    int val = 0, bits = -8;
    for (const auto &c : s)
    {
        if (vec[c] == -1) break;
        val = (val << 6) + vec[c];
        bits += 6;

        if (bits >= 0)
        {
            ret.push_back(char((val >> bits) & 0xFF));
            bits -= 8;
        }
    }

    return ret;
}
