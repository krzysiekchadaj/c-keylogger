#include <iostream>
#include <fstream>
#include <string>

#include "Decrypt.h"

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::cout << "ERROR: program needs two arguments: <input> <output" << std::endl;
        return 2;
    }

    std::string in(argv[1]);
    std::string out(argv[2]);

    std::ifstream fi(in);
    if (!fi)
    {
        std::cout << "ERROR: cannot read input file: '" << in << "'" << std::endl;
        return 3;
    }

    std::string data;
    fi >> data;
    if (!fi)
    {
        std::cout << "ERROR: input file '" << in << "' corrupted." << std::endl;
        return 4;
    }

    if (fi.is_open())
    {
        fi.close();
    }

    data = Base64::Decrypt(data);

    std::ofstream fo(out);
    if (!fo)
    {
        std::cout << "ERROR: cannot create output file: '" << out << "'" << std::endl;
        return 5;
    }

    fo << data;
    if (!fo)
    {
        std::cout << "ERROR: output file '" << out << "' corrupted." << std::endl;
        return 6;
    }

    if (fo.is_open())
    {
        fo.close();
    }

    std::cout << "Decoding successful" << std::endl;
    return 0;
}
