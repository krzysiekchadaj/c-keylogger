#include "AppLog.h"

#include <fstream>

#include "Time.h"

void AppLog::Write(const std::string & s)
{
#ifdef _DEBUG
    std::ofstream file("AppLog.txt", std::ios::app);
    if (file.is_open())
    {
        file << "[" << Time::Get() << "] " << s << std::endl;
        file.close();
    }
#endif
}
