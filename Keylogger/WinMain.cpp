#include <Windows.h>

#include "File.h"
#include "Hook.h"

int main()
{
    File::MakeFullDirectory(File::GetLogsPath());

    Hook::Install();

    MSG Msg;
    while (GetMessage(&Msg, NULL, 0, 0))
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

    Hook::Uninstall();
    return 0;
}
