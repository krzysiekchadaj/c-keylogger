#ifndef LOG_H
#define LOG_H

#include <string>

namespace Log
{
    std::string Write(const std::string &message);
}

#endif
