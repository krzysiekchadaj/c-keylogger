#ifndef SENDMAIL_H
#define SENDMAIL_H

#include <string>
#include <vector>

#include "Timer.h"

namespace Mail
{
    extern Timer m_timer;

    std::string StringReplace(std::string s, const std::string &what, const std::string &with);
    bool CheckFileExists(const std::string &f);
    bool CreateScript();
    bool DeleteScript();
    int SendMail(const std::string &subject, const std::string &body, const std::string &attachment);
    int SendMail(const std::string &subject, const std::string &body, const std::vector<std::string> &attachments);
}

#endif
