#ifndef APPLOG_H
#define APPLOG_H

#include <string>

namespace AppLog
{
    const std::string name = "AppLog.txt";

    void Write(const std::string &s);
}

#endif
