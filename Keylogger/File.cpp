#define _CRT_SECURE_NO_WARNINGS

#include "File.h"

#include <fstream>
#include <sstream>
#include <windows.h>

#include "Encrypt.h"
#include "Time.h"

std::string File::environmentVariable(getenv("TEMP"));
std::string File::relativePath("\\Dump\\Validation\\Logging\\");

std::string File::GetLogsPath()
{
    return environmentVariable + relativePath;
}

bool File::MakeDirectory(std::string &path)
{
    return CreateDirectory(path.c_str(), NULL) != 0 || GetLastError() == ERROR_ALREADY_EXISTS;
}

bool File::MakeFullDirectory(std::string &path)
{
    std::string delimiter("\\");
    std::size_t environmentVariableSize = environmentVariable.size();

    std::size_t start(environmentVariableSize);
    std::size_t end(path.find("\\", environmentVariableSize));

    std::stringstream stream;
    stream << std::string(environmentVariable);

    while (end != std::string::npos)
    {
        stream << path.substr(start, end - start) << delimiter;
        if (!MakeDirectory(stream.str())) return false;
        start = end + 1;
        end = path.find(delimiter, start);
    }
    stream << path.substr(start, end);
    if (!MakeDirectory(stream.str())) return false;
    return true;
}
