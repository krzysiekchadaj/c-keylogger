#include "Timer.h"

void Timer::SleepAndRun()
{
    std::this_thread::sleep_for(interval);
    if (isAlive)
        function();
}

void Timer::ThreadFuncion()
{
    if (repeatCount == Infinite)
    {
        while (isAlive)
            SleepAndRun();
    }
    else
    {
        while (leftCount--)
            SleepAndRun();
    }
}

Timer::Timer()
{
    // empty
}

Timer::Timer(const std::function <void(void)> &function) : function(function)
{
    // empty
}

Timer::Timer(const std::function <void(void)> &function, const unsigned long &interval, const long repeat)
    : function(function), interval(std::chrono::milliseconds(interval)), repeatCount(repeat)
{
    // empty
}

void Timer::Start(bool Async)
{
    if (IsAlive())
        return;

    isAlive = true;
    leftCount = repeatCount;
    if (Async)
        thread = std::thread(&Timer::ThreadFuncion, this);
    else
        ThreadFuncion();
}

void Timer::Stop()
{
    isAlive = false;
    thread.join();
}

bool Timer::IsAlive() const
{
    return isAlive;
}

void Timer::SetFunction(const std::function <void(void)> &func)
{
    function = func;
}

long Timer::GetRepeatCount() const
{
    return repeatCount;
}

void Timer::SetRepeatCount(const long repeat)
{
    if (isAlive)
        return;
    repeatCount = repeat;
}

long Timer::GetLeftCount() const
{
    return leftCount;
}

void Timer::SetInterval(const unsigned long &interv)
{
    if (isAlive)
        return;
    interval = std::chrono::milliseconds(interv);
}
