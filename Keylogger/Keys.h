#ifndef KEYS_H
#define KEYS_H

#include <map>
#include <string>

class KeyPair
{
private:
    std::string VKName;
    std::string Name;

public:
    KeyPair();
    KeyPair(const std::string &vk, const std::string &name);	

    std::string getVKName();
    std::string getName();
};

class Keys
{
public:
    static std::map<int, KeyPair> KEYS;
};

#endif
