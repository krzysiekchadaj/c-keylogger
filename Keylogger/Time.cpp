#define _CRT_SECURE_NO_WARNINGS

#include "Time.h"

#include <iomanip>
#include <sstream>

std::string Time::Get()
{
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::ostringstream oss;
    oss << std::put_time(&tm, "%Y-%m-%d-%H-%M-%S");
    return oss.str();
}
