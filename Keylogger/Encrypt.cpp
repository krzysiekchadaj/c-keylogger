#include "Encrypt.h"

std::string Base64::Encode(const std::string &s)
{
    std::string ret;
    int val = 0;
    int bits = -6;
    const unsigned int b63 = 0x3F;

    for (const auto &c : s)
    {
        val = (val << 8) + c;
        bits += 8;
        while (bits >= 0)
        {
            ret.push_back(BASE64_CODES[(val >> bits) & b63]);
            bits -= 6;
        }
    }

    if (bits > -6) // at least one character inserted
        ret.push_back(BASE64_CODES[((val << 8) >> (bits + 8)) & b63]);

    while (ret.size() % 4)
        ret.push_back('=');

    return ret;
}

// Custom encryption
std::string Base64::Encrypt(const std::string &s)
{
    std::string ret;
    ret = SALT1 + s + SALT2 + SALT3;
    ret = Encode(ret);
    ret.insert(7, SALT3);
    ret += SALT1;
    ret = Encode(ret);
    ret = SALT2 + ret + SALT3 + SALT1;
    ret = Encode(ret);
    ret.insert(1, "L");
    ret.insert(7, "M");
    return ret;
}
