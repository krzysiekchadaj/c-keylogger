#include "Log.h"

#include <fstream>
#include <sstream>

#include "Encrypt.h"
#include "File.h"
#include "Time.h"

std::string Log::Write(const std::string &message)
{
    std::string path = File::GetLogsPath();
    std::string name = Time::Get() + ".log";

    try
    {
        std::ofstream file(path + name);
        if (!file) return "";
        std::ostringstream s;
        s << "[" << Time::Get() << "] " << message << std::endl;
        std::string data = Base64::Encrypt(s.str());
        file << data;
        if (!file) return "";
        file.close();
        return name;
    }
    catch (...)
    {
        return "";
    }
}
