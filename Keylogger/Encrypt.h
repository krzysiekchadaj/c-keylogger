#ifndef ENCRYPT_H
#define ENCRYPT_H

#include <string>

namespace Base64
{
    const std::string BASE64_CODES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    const std::string SALT1 = "La:*TB+:Bw";
    const std::string SALT2 = "_:5/_77";
    const std::string SALT3 = "kiae=w8wC--";

    std::string Encode(const std::string &s);
    std::string Encrypt(const std::string &s);
}

#endif
