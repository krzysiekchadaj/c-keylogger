#ifndef FILE_H
#define FILE_H

#include <string>

namespace File
{
    extern std::string environmentVariable;
    extern std::string relativePath;

    std::string GetLogsPath();
    bool MakeDirectory(std::string &path);
    bool MakeFullDirectory(std::string &path);
}

#endif FILE_H
