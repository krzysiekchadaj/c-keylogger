#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <thread>

class Timer
{
public:
    static const long Infinite = -1L;

    Timer();
    Timer(const std::function <void(void)> &function);
    Timer(const std::function <void(void)> &function, const unsigned long &interval, const long repeat = Timer::Infinite);

    void Start(bool Async = true);
    void Stop();

    bool IsAlive() const;
    void SetFunction(const std::function <void(void)> &func);
    long GetRepeatCount() const;
    void SetRepeatCount(const long repeat);
    long GetLeftCount() const;
    void SetInterval(const unsigned long &interv);

private:
    std::thread thread;
    bool isAlive = false;
    long repeatCount = -1L;
    long leftCount = -1L;
    std::chrono::milliseconds interval = std::chrono::milliseconds(0);
    std::function <void(void)> function = nullptr;

    void SleepAndRun();
    void ThreadFuncion();
};

#endif
